# Clojure Sketching

Quil, Treemaps, PlantUML.

## License

### Code

Copyright © 2020 Daniel Gregoire

Licensed under the [Mozilla Public License Version
2.0](https://www.mozilla.org/en-US/MPL/2.0/).

### Font

The Fantasque Sans Mono font was created by Jany Belluz <jany.belluz AT
hotmail.fr> and is licensed under the SIL Open Font License (see
[LICENSE.txt](https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/FantasqueSansMono/LICENSE.txt)).
