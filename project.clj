(defproject com.semperos/sketch "0.1.0-SNAPSHOT"
  :description "Sketches of Data using Clojure"
  :url "https://gitlab.com/semperos/sketch"
  :license {:name "Mozilla Public License 2.0"
            :url "https://www.mozilla.org/en-US/MPL/2.0/"}
  :dependencies [[quil "3.1.0"]
                 [net.sourceforge.plantuml/plantuml "1.2020.9"]
                 [com.phronemophobic/treemap-clj "0.2.0"]]
  :profiles {:dev {:dependencies [[org.clojure/clojure "1.10.1"]]
                   :resource-paths ["resources"]}})
