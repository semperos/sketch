(ns sketch.core
  "Sketch driven by an org-mode table of a scenario describing the
  Billing escalation of 2020-07-07.

  Made with quil version 3.1.0"
  (:require
   [clojure.java.io :as io]
   [clojure.pprint :as pprint]
   [clojure.string :as str]
   [quil.core :as q]
   [quil.middleware :as m]))

(defn dates [header]
  (let [header (map #(Integer/parseInt %) (next header))
        [jun nxt] (split-with (partial < 1) header)
        [jul aug] (split-with (partial > 31) nxt)]
    (concat
     (map (partial str "Jun ") jun)
     (map (partial str "Jul ") jul)
     (map (partial str "Aug ") aug))))

(defn cells [line]
  (into []
        (comp
         (map str/trim))
        (next (str/split (str/trim line) #"\|"))))

(def pos (atom 0))
;; Need to have the correct folder added to the classpath.
(def table (map cells (str/split-lines (slurp (io/resource "data.org")))))
(def length (count (next (first table))))

(def dataset
  (let [rows (next table)
        row-headers (map (comp keyword first) rows)
        dates (dates (first table))]
    (map-indexed
     (fn [idx date]
       [date (into {} (map (fn [row-header row]
                             (let [row (next row) ;; skip row header
                                   value (nth row idx)
                                   value (when-not (str/blank? value)
                                           (if-not (#{:invoice :actual-call :expected-call} row-header)
                                             (Integer/parseInt value)
                                             value))]
                               [row-header value]))
                           row-headers rows))])
     dates)))

(defn setup []
  ;; Set frame rate to 30 frames per second.
  (q/text-font (q/create-font "FantasqueSansMonoNerdFontCompleteM-Regular" 25 true))
  (q/frame-rate 50)
  ;; Set color mode to HSB (HSV) instead of default RGB.
  ;; (q/color-mode :rgb 255)
  ;; setup function returns initial state. It contains
  ;; circle color and position.
  {:pos 0
   :color (q/color 255 0 0)
   :angle 0})

(def key-still-pressed? (atom false))

(defn update-pos [pos]
  (if (q/key-pressed?)
    (if @key-still-pressed?
      pos
      (let [return (case (q/key-as-keyword)
                     :right (if (< pos (dec length))
                              (inc pos)
                              pos)
                     :left (if (> pos 0)
                             (dec pos)
                             pos)
                     pos)]
        (reset! key-still-pressed? true)
        return))
    (do
      (reset! key-still-pressed? false)
      pos)))

(defn update-state [state]
  (update state :pos update-pos))

(defn draw-person [x y op]
  (let [x #(+ % x)
        y #(+ % y)
        hor (fn [] (q/rect (x 138) (y 92) 70 5))
        ver (fn [] (q/rect (x 170) (y 60) 5 70))]
    (q/ellipse (x 100) (y 100) 60 75)
    (q/triangle (x 50) (y 220) (x 100) (y 120) (x 150) (y 220))
    (case op
      :add (do (hor) (ver))
      (:remove :subtract) (hor))))

(defn parse-call [s]
  (let [special-case? ({\F false \T true} (last s))]
    (str (when special-case? "Special-Case ")
         (apply str (butlast s)))))


(defn draw-state [{:keys [pos color] :as state}]
  (q/background 240)
  ;; Set circle color.
  (let [box (atom {:value 10
                   :previous-x 0})
        pad #(+ % 5)]
    (doseq [[idx [date m]] (map-indexed #(vector %1 %2) dataset)
            :let [x (mod (* idx 50) 600)
                  ;; TODO y
                  y (if (< x (:previous-x @box))
                      (:value (swap! box update :value (partial + 50)))
                      (:value @box))
                  _ (swap! box assoc :previous-x x)
                  [_ previous] (when (pos? idx) (nth dataset (dec idx)))
                  prev-spots (:spots previous)
                  gray (q/color 210)
                  box-color (if-let [invoice (:invoice m)]
                              (case invoice
                                "PRIM" (q/color 0 255 0 90)
                                "ADJ" (q/color 255 204 0 90)
                                (q/color 0 0 255 90))
                              gray)
                  box-color (if (and prev-spots (> (:spots m) prev-spots))
                              (q/color 139 120 250)
                              box-color)
                  box-color (if (and prev-spots (< (:spots m) prev-spots))
                              (q/color 139 120 250 80)
                              box-color)
                  box-color (if (not= (:expected-call m) (:actual-call m))
                              (q/color 255 0 0 90)
                              box-color)
                  radius (if (not= (:expected-high-val m) (:actual-high-val m))
                           40
                           3)
                  selected? (= idx pos)]]
      (q/fill box-color)
      (if selected?
        (q/stroke-weight 3)
        (q/stroke-weight 1))
      (q/rect (pad x) y 42 40 radius)
      (q/fill (q/color 0 0 0))
      (q/text-size 12)
      (q/text date (+ 3 (pad x)) (+ y 20))
      (when (= date "Jul 7")
        (q/fill (q/color 255 0 0))
        (q/text-size 25)
        (q/text "×" (+ 14 (pad x)) (+ y 35)))
      (when (= date "Aug 5")
        (q/fill (q/color 34 139 34))
        (q/text-size 25)
        (q/text "✓" (+ 14 (pad x)) (+ y 35)))
      (when selected?
        (q/stroke-weight 1)
        (q/fill box-color)
        (q/rect (pad 10) 280 600 300 2)
        (q/fill 0 0 0)
        (q/text-size 25)
        (q/text date (pad 30) 320)
        (cond
          (= "PRIM" (:invoice m))
          (do
            (q/text-size 35)
            (q/text "New Period"
                    (pad 140) 355)
            (q/text-size 70)
            (q/text "$$$"
                    (pad 250) 450)
            (q/text-size 35)
            (q/text (str "High Value: " (:actual-high-val m))
                    (pad 130) 535))

          (= "ADJ" (:invoice m))
          (do
            (q/text-size 35)
            (q/text "Interim"
                    (pad 140) 355)
            (q/text-size 70)
            (q/text "$"
                    (pad 290) 450)
            (q/text-size 35)
            (if (< 12 idx 43)
              (q/text (str "[BUG] High Value: " (:actual-high-val m))
                      (pad 90) 535)
              (q/text (str "High Value: No Changes")
                      (pad 70) 535)))

          (and prev-spots
               (> (:spots m) prev-spots))
          (do
            (draw-person 370 280 :add)
            (cond
              (< (:actual-high-val m) (:expected-high-val m))
              (do
                (q/text (str "Bug High Value: " (:actual-high-val m))
                        (pad 50) 370)
                (q/text (str "Previous High Value: " (:expected-high-val m))
                        (pad 50) 420)
                (q/text (str "API Call (!): " (parse-call (:actual-call m)))
                        (pad 50) 470))

              (not= (:actual-call m) (:expected-call m))
              (do
                (q/text (str "High Value: " (:actual-high-val m))
                        (pad 50) 370)
                (q/text (str "API Call (!!): " (parse-call (:actual-call m)))
                        (pad 50) 420))

              :else
              (do
                (q/text (str "High Value: " (:actual-high-val m))
                        (pad 50) 370)
                (q/text (str "API Call: " (parse-call (:actual-call m)))
                        (pad 50) 420))))

          (and prev-spots
               (< (:spots m) prev-spots))
          (do
            (draw-person 370 280 :remove)
            (q/text (str "High Value: " (:actual-high-val m))
                    (pad 50) 370)
            (q/text (str "Actual Value: " (:spots m))
                    (pad 50) 420)
            (when (> idx 12)
              (q/text (str "API Call: " (parse-call (:actual-call m)))
                      (pad 50) 470)))

          :else
          (do
            (q/text (str "High Value: " (:actual-high-val m))
                    (pad 50) 370)
            (q/text (str "Actual Value: " (:spots m))
                    (pad 50) 420)))))))

(q/defsketch sketch
  :title "Analysis"
  :size [630 600]
  :resizable true
  ;; setup function called only once, during sketch initialization.
  :setup setup
  ;; update-state is called on each iteration before draw-state.
  :update update-state
  :draw draw-state
  :features [:keep-on-top]
  ;; This sketch uses functional-mode middleware.
  ;; Check quil wiki for more info about middlewares and particularly
  ;; fun-mode.
  :middleware [m/fun-mode])
