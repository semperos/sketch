(ns sketch.flow
  {:clj-kondo/config
   '{:lint-as {quil.core/defsketch clj-kondo.lint-as/def-catch-all}}}
  (:require
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [clojure.pprint :as pprint]
   [clojure.string :as str]
   [quil.core :as q]
   [quil.middleware :as m])
  (:import
   (java.io File)
   (java.nio.charset Charset)
   (java.security MessageDigest)
   (javax.xml.bind DatatypeConverter)
   (net.sourceforge.plantuml SourceStringReader)))

;;;;;;;;;;;;;;
;; PlantUML ;;
;;;;;;;;;;;;;;
(defn fmt [fmt & args]
  (let [args (mapv (fn [x] (if (instance? clojure.lang.Named x) (name x) x)) args)]
    (apply format fmt args)))

(defn- emit-puml-expr-dispatch
  [[tag & _rest]]
  (if (#{\. \-} (first (name tag)))
    :>  ;; parse further in :> defmethod
    tag))

(defmulti emit-puml-expr #'emit-puml-expr-dispatch)

;;;;;;;;;;;;;;;;;;;;;;;;
;; PlantUML Use Cases ;;
;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod emit-puml-expr :actor
  [[_tag actor]]
  (if (vector? actor)
    (let [[actor-id actor-name] actor]
      (fmt "actor \"%s\" as %s" actor-name actor-id))
    (fmt "actor %s" actor)))

(defmethod emit-puml-expr :use-case
  [[_tag use-case-id use-case-name]]
  (fmt "usecase \"%s\" as %s" use-case-name use-case-id))

(defmethod emit-puml-expr :> ;; see dispatch fn
  [[tag left-node right-node]]
  ;; Pass-through for now
  (fmt "%s %s %s" left-node tag right-node))

(defmethod emit-puml-expr :direction
  [[_tag direction]]
  (format "%s direction"
          (case direction
            :ltr "left to right"
            :ttb "top to bottom")))

(defmethod emit-puml-expr :use-case
  [[_tag [use-case-id use-case-name]]]
  (format "usecase %s as %s" use-case-name use-case-id))

(defmethod emit-puml-expr :package
  [[_tag package-name & use-cases]]
  (let [use-cases (str/join "\n" (mapv emit-puml-expr use-cases))]
    (format "package %s {\n%s\n}" package-name use-cases)))

(defmethod emit-puml-expr :rectangle
  [[_tag rectangle-name & use-cases]]
  (let [use-cases (str/join "\n" (mapv emit-puml-expr use-cases))]
    (format "rectangle %s {\n%s\n}" rectangle-name use-cases)))

;;;;;;;;;;;;;;;;;;;;;;;
;; PlantUML Activity ;;
;;;;;;;;;;;;;;;;;;;;;;;

(defmethod emit-puml-expr :start
  [_]
  "start")

(defmethod emit-puml-expr :stop
  [_]
  "stop")


(defmethod emit-puml-expr :act
  [[_tag action & [modifier]]]
  (format ":%s%s" action (or modifier \;)))

(defmethod emit-puml-expr :pred
  [[_pred predicate]]
  (format "(%s)" predicate))

(defmethod emit-puml-expr ::if-branch
  [[tag label & then-body]]
  (if (string? label)
    (format "%s (%s)\n%s"
            (name tag)
            label
            (->> (map emit-puml-expr then-body)
                 (str/join "\n")))
    (let [then-body (cons label then-body)]
      (format "%s\n%s"
              (name tag)
              (->> (map emit-puml-expr then-body)
                   (str/join "\n"))))))

(derive ::then ::if-branch)
(derive ::else ::if-branch)

(defmethod emit-puml-expr :then
  [args]
  (emit-puml-expr (assoc args 0 ::then)))

(defmethod emit-puml-expr :else
  [args]
  (emit-puml-expr (assoc args 0 ::else)))

(defmethod emit-puml-expr :else-if
  [[_tag pred then]]
  (format "elseif %s then %s\n"
          (emit-puml-expr pred)
          (emit-puml-expr then)))

(defn emit-puml-if-tail
  "The tail of an if is 0-or-more elseif's and 0-or-1 else's."
  [if-tail]
  (->> (map emit-puml-expr if-tail)
       (str/join "\n")))

(defmethod emit-puml-expr :if
  [[_if & stuff]]
  (let [[pred then & more-stuff] stuff
        pred (emit-puml-expr pred)
        then (emit-puml-expr then)]
    (if-not (seq more-stuff)
      (format "if %s %s\nendif" pred then)
      (format "if %s %s\n%s\nendif" pred then (emit-puml-if-tail more-stuff)))))

(defmethod emit-puml-expr :default
  [[tag & _args]]
  (format "(%s)" tag))

(defn emit-puml*
  [pedn]
  (into []
        (map emit-puml-expr)
        pedn))

(defn emit-puml
  "Expects a collection of PlantUML EDN expressions."
  [pedn]
  (apply str
         "@startuml\n"
         (str/join "\n" (emit-puml* pedn))
         "\n@enduml\n"))

(def example-activity-str
  "
@startuml

start
:ClickServlet.handleRequest();
:new page;
if (Page.onSecurityCheck) then (true)
  :Page.onInit();
  if (isForward?) then (no)
    :Process controls;
    if (continue processing?) then (no)
      stop
    endif

    if (isPost?) then (yes)
      :Page.onPost();
    else (no)
      :Page.onGet();
    endif
    :Page.onRender();
  endif
else (false)
endif

if (do redirect?) then (yes)
  :redirect process;
else
  if (do forward?) then (yes)
    :Forward request;
  else (no)
    :Render page template;
  endif
endif

stop

@enduml
")

(def example-activity
  [[:start]
   [:act "ClickServlet.handleRequest()"]
   [:act "new page"]
   [:if [:pred "Page.onSecurityCheck"]
    [:then "true"
     [:act "Process controls" \|]
     [:if [:pred "continue processing?"]
      [:then "yes"
       [:act "Page.onPost()"]]
      [:else "no"
       [:act "Page.onGet()"]]]
     [:act "Page.onRender()"]]
    [:else "false"]]

   [:if [:pred "do redirect?"]
    [:then "yes"
     [:act "redirect process" \<]]
    ]
   [:stop]])

(def example-use-case
  ;; TODO need to emit-puml-expr for atoms like (checkout)
  (let [ch "[checkout]"
        pmt "(payment)"
        help "(help)"]
    [[:direction :ltr]
     [:actor [:cus :customer]]
     [:actor :cashier]
     [:rectangle :checkout
      [:-- :cus  ch]
      [:.>  ch   pmt]
      [:.>  help ch]
      [:--  ch   :cashier]]
     ]))

(defn setup []
  ;; Set frame rate to 30 frames per second.
  (q/text-font (q/create-font "FantasqueSansMonoNerdFontCompleteM-Regular" 25 true))
  (q/frame-rate 50)
  ;; Set color mode to HSB (HSV) instead of default RGB.
  ;; (q/color-mode :rgb 255)
  ;; setup function returns initial state. It contains
  ;; circle color and position.
  {:pos 0
   :flowcharts {}
   :choice :example})

(def md5 (MessageDigest/getInstance "MD5"))
(def charset-name "UTF-8")
(def charset (Charset/forName charset-name))

(defn render-flowchart
  ([plantuml-source-code]
   (render-flowchart (DatatypeConverter/printHexBinary (.digest md5 (.getBytes plantuml-source-code charset)))
                     plantuml-source-code))
  ([file-name plantuml-source-code]
   (let [destination-file (File. file-name)]
     (with-open [png (io/output-stream destination-file)]
       (let [ssr (SourceStringReader. plantuml-source-code)]
         (.outputImage ssr png)))
     destination-file)))

(defn update-state [state]

  (if-not (get-in state [:flowcharts :example])
    (let [file (render-flowchart (try
                                   (emit-puml example-activity)
                                   (catch Exception e
                                     (prn e)
                                     "@startuml\n(Broken)\n@enduml")))
          quil-img (q/load-image (.getAbsolutePath file))]
      (-> state
          (assoc-in [:flowcharts :example :image] quil-img)
          (assoc-in [:flowcharts :example :file] file)))
    state))

(defn draw-state [{:keys [choice flowcharts _pos] :as _state}]
  (q/background 240)
  (when-let [flowchart (get flowcharts choice)]
    (let [{:keys [image file]} flowchart]
      (q/image image 0 0)
      ;; (q/fill 0)
      ;; (q/text (.getAbsolutePath file) 10 500)
      )))

(defn edn->puml
  [file-name]
  (emit-puml (edn/read-string (slurp file-name))))

(defn render-all []
  [#_(render-flowchart "alpha.png" (emit-puml example-activity))
   (render-flowchart "trial.png" (edn->puml "/Users/dlg/clubhouse/flowchart-trial.edn"))])

#_(pprint/pprint (render-all))
(q/defsketch sketch
  :title "Flowchart"
  :size (let [dim (.getScreenSize (java.awt.Toolkit/getDefaultToolkit))
              w (.getWidth dim)
              h (.getHeight dim)]
          [w h])
  :resizable true
  ;; setup function called only once, during sketch initialization.
  :setup setup
  ;; update-state is called on each iteration before draw-state.
  :update update-state
  :draw draw-state
  :features [:keep-on-top]
  ;; This sketch uses functional-mode middleware.
  ;; Check quil wiki for more info about middlewares and particularly
  ;; fun-mode.
  :middleware [m/fun-mode])
