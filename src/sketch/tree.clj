(ns sketch.tree
  "https://github.com/phronmophobic/treemap-clj"
  (:require
   [membrane.java2d :as java2d]
   [treemap-clj.core :as treemap]
   [treemap-clj.view :as view]))

(def eg [])

(comment
  (def tm (treemap/treemap eg (treemap/make-rect 2000 2000)))
  (def tmr [(view/render-depth tm)
            (view/render-hierarchy-lines tm)
            (view/render-value-labels tm)])
  (java2d/save-to-image! "treemap.png" tmr)

  )
